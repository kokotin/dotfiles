--Treesitter
require'nvim-treesitter.configs'.setup {
  ensure_installed = {
    "bash",
    "c",
    "cmake",
    "comment",
    "commonlisp",
    "cpp",
    "css",
    "dockerfile",
    "go",
    "graphql",
    "hjson",
    "html",
    "http",
    "json",
    "json5",
    "jsonc",
    "lua",
    "make",
    "markdown",
    "python",
    "query",
    "regex",
    "scss",
    "toml",
    "vim",
    "yaml"
  }, -- one of "all", "maintained" (parsers with maintainers), or a list of languages
  -- ignore_install = { "javascript" }, -- List of parsers to ignore installing
  highlight = {
    enable = true,              -- false will disable the whole extension
    -- disable = { "c", "rust" },  -- list of language that will be disabled
  },
  indent = {
    enable = false
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn",
      node_incremental = "grn",
      scope_incremental = "grc",
      node_decremental = "grm",
    },
  },
  rainbow = {
    enable = true,
    extended_mode = true, -- Highlight also non-parentheses delimiters, boolean or table: lang -> boolean
    max_file_lines = 1000, -- Do not enable for files with more than 1000 lines, int
    colors = { "#f1fa8c", "#ff5555", "#bd93f9", "#ff79c6", "#ffb86c", "#50fa7b", "#8be9fd"}
  }
}
