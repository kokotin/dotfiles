vim.opt.completeopt = "menuone,noselect"
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.hidden = true
vim.opt.termguicolors = true
vim.opt.expandtab = true
vim.opt.cursorline = true
vim.opt.scrolloff = 8
vim.opt.list = true
vim.opt.listchars.tab ='›'
vim.opt.listchars.trail = '•'
vim.opt.listchars.extends = '#'
vim.opt.listchars.nbsp = '.'
vim.opt.wrap = false
vim.opt.mouse = 'a'
vim.opt.hlsearch = false
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.modeline = true
vim.cmd('color dracula')
