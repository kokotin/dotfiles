vim.cmd("let g:indent_blankline_context_patterns = ['^call', '^string', '^dict', '^list', '^tuple', '^while', '^if', '^for', '^try', '^with', 'class', 'function', 'method']")

require("indent_blankline").setup {
    char = "|",
    buftype_exclude = {"terminal"},
    show_current_context = true,
}
