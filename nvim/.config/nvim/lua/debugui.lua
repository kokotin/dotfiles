require("dapui").setup{
sidebar = {
    elements = {
      { id = "stacks", size = 0.25 },
      { id = "watches", size = 00.25 },
      { id = "breakpoints", size = 0.25 },
      { id = "scopes", size = 0.25 },
    },
    -- size = 40,
    -- position = "left", -- Can be "left", "right", "top", "bottom"
  },
}
