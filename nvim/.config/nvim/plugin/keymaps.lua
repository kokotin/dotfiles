-- Prevent CTRL+Z suspending Vim
vim.keymap.set("n", "<C-Z>", "", {silent = true, noremap = true})

-- command-line mode
-- vim.api.nvim_set_keymap("n", "", "", {silent = true, noremap = true})
-- nnoremap <space> :
-- vnoremap <space> :

-- undo break points
vim.keymap.set("i", ",", ",<C-G>u", {silent = false, noremap = true})
vim.keymap.set("i", ".", ".<C-G>u", {silent = false, noremap = true})
vim.keymap.set("i", "<space>", "<space><C-G>u", {silent = false, noremap = true})

-- ctrl+s save
vim.keymap.set("n", "<C-S>", "<cmd>update<CR>", {silent = true, noremap = true})
vim.keymap.set("v", "<C-S>", "<C-C><cmd>update<CR><ESC>", {silent = true, noremap = true})
vim.keymap.set("i", "<C-S>", "<C-O><cmd>update<CR><ESC>", {silent = true, noremap = true})

-- greatest remap ever
-- vim.keymap.set("v", "<leader>p", "_dP", {silent = false, noremap = true})

-- next greatest remap ever
vim.keymap.set("n", "<leader>y", "\"+y", {silent = false, noremap = true})
vim.keymap.set("v", "<leader>y", "\"+y", {silent = false, noremap = true})
vim.keymap.set("n", "<leader>Y", "\"+Y", {silent = false, noremap = true})
vim.keymap.set("n", "<leader>d", "_d", {silent = false, noremap = true})
vim.keymap.set("v", "<leader>d", "_d", {silent = false, noremap = true})

-- keeping it centered
vim.keymap.set("n", "n", "nzzzv", {silent = true, noremap = true})
vim.keymap.set("n", "N", "Nzzzv", {silent = true, noremap = true})
vim.keymap.set("n", "J", "mzJ`z", {silent = true, noremap = true})

-- move line(s) up/down in visual mode
-- use = or == ident
vim.keymap.set("x", "J", ":move '>+1<CR>gv-gv", {silent = false, noremap = true})
vim.keymap.set("x", "K", ":move '<-2<CR>gv-gv", {silent = false, noremap = true})

-- neogen (documentation)
vim.keymap.set("n", "<leader>nf", "<cmd>lua require('neogen').generate()<CR>", {silent = true, noremap = true})
vim.keymap.set("n", "<leader>nc", "<cmd>lua require('neogen').generate({ type = 'class' })<CR>", {silent = true, noremap = true})

if not vim.g.vscode then
  -- Lsp key bindings /lspsaga
  -- lsp provider to find the cursor word definition and reference
  vim.keymap.set("n", "<leader>lr", "<cmd>Lspsaga rename<cr>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>la", "<cmd>Lspsaga code_action<cr>", {silent = true, noremap = true})
  vim.keymap.set("x", "<leader>la", ":<c-u>Lspsaga range_code_action<cr>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>lk",  "<cmd>Lspsaga hover_doc<cr>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>lh", "<cmd>Lspsaga show_line_diagnostics<cr>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>l[", "<cmd>Lspsaga diagnostic_jump_next<cr>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>l]", "<cmd>Lspsaga diagnostic_jump_prev<cr>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>lf", "<cmd>Lspsaga lsp_finder<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>ls", "<cmd>Lspsaga signature_help<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>ld", "<cmd>Lspsaga preview_definition<CR>", {silent = true, noremap = true})

  -- debuger
  vim.keymap.set("n", "<F5>", "<cmd>lua require'dap'.continue()<CR>", {silent = true, noremap = false})
  vim.keymap.set("n", "<F6>", "<cmd>lua require'dapui'.toggle()<CR>", {silent = true, noremap = false})
  vim.keymap.set("n", "<F9>", "<cmd>lua require'dap'.toggle_breakpoint()<CR>", {silent = true, noremap = false})
  vim.keymap.set("n", "<F10>", "<cmd>lua require'dap'.step_over()<CR>", {silent = true, noremap = false})
  vim.keymap.set("n", "<F11>", "<cmd>lua require'dap'.step_into()<CR>", {silent = true, noremap = false})

  -- git
  vim.keymap.set("n", "<leader>gj", "<cmd>diffget //3<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>gf", "<cmd>diffget //2<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>gs", "<cmd>G<CR>", {silent = true, noremap = true})

  -- Find files using Telescope command-line sugar.
  vim.keymap.set("n", "<leader>ff", "<cmd>lua require('telescope.builtin').find_files()<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>fg", "<cmd>lua require('telescope.builtin').live_grep()<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>fb", "<cmd>lua require('telescope.builtin').buffers()<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>fh", "<cmd>lua require('telescope.builtin').help_tags()<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<leader>fo", "<cmd>lua require'telescope.builtin'.oldfiles{}<CR>", {silent = true, noremap = true})

  -- nvim tree
  vim.keymap.set("n", "<F1>", ":NvimTreeToggle<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<F2>", ":NvimTreeFindFileToggle<CR>", {silent = true, noremap = true})

  -- UNDOTree configuration
  -- vim.keymap.set("n", "<F3>", "<cmd>UndotreeToggle<CR>", {silent = true, noremap = true})

  -- esc remap
  -- vim.keymap.set("i", "jl", "<esc>", {silent = true, noremap = true})
  -- vim.keymap.set("v", "jl", "<esc>", {silent = true, noremap = true})

  -- resize
  vim.keymap.set("n", "<Up>", "<cmd>resize -2<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<Down>", "<cmd>resize +2<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<Left>", "<cmd>vertical resize -2<CR>", {silent = true, noremap = true})
  vim.keymap.set("n", "<Right>", "<cmd>vertical resize +2<CR>", {silent = true, noremap = true})
end

--[[
" compe
inoremap <silent> <expr> <C-Space> compe#complete()
inoremap <silent> <expr> <CR>      compe#confirm(luaeval("require 'nvim-autopairs'.autopairs_cr()"))
inoremap <silent> <expr> <CR>      compe#confirm('<CR>')
inoremap <silent> <expr> <C-e>     compe#close('<C-e>')
inoremap <silent> <expr> <C-f>     compe#scroll({ 'delta': +4 })
inoremap <silent> <expr> <C-d>     compe#scroll({ 'delta': -4 })
inoremap <silent> <expr> <TAB>     pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <silent> <expr> <S-TAB>   pumvisible() ? "\<C-p>" : "\<C-h>"
]]
