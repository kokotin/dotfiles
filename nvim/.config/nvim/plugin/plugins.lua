-- pkgs location ~/.local/share/nvim/site/pack/packer/
-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function()
  -- Packer can manage itself
  use {'wbthomason/packer.nvim'}
  use {'numToStr/Comment.nvim'}
  use {'danymat/neogen', branch = 'main'}
  use {'mfussenegger/nvim-dap'}
  use {'rcarriga/nvim-dap-ui'}

  use {'tpope/vim-fugitive', opt = true, cmd = 'Git'}
  use {'easymotion/vim-easymotion'}
  use {'tpope/vim-surround'}
  use {'machakann/vim-swap'}
  use {'dracula/vim', as = 'dracula'}

  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
  use {'romgrk/nvim-treesitter-context'}
  use {'nvim-treesitter/playground', opt = true, cmd = ':TSPlaygroundToggle'}
  use {'tami5/lspsaga.nvim'}
  use {'neovim/nvim-lspconfig'}

	use { "williamboman/mason-lspconfig.nvim", branch = 'main' }
  use { "williamboman/mason.nvim", branch = 'main' }

  use { "beauwilliams/focus.nvim", config = function() require("focus").setup() end }
  use {'folke/which-key.nvim', branch = 'main'}
  use {'kyazdani42/nvim-tree.lua'}
  use {'kyazdani42/nvim-web-devicons'}
  use {'p00f/nvim-ts-rainbow'}

  use {'windwp/nvim-autopairs'}
  use {'abecodes/tabout.nvim'}
  use {'lukas-reineke/indent-blankline.nvim'}
  use {'nvim-lualine/lualine.nvim'}
  use {'folke/zen-mode.nvim', opt = true, cmd = ':ZenMode'}

  -- use {'sbdchd/neoformat', otp = true, ft = 'python'}
  use {'mhartington/formatter.nvim'}
  use {'mfussenegger/nvim-dap-python'}
  use {'L3MON4D3/LuaSnip'}
  use {'saadparwaiz1/cmp_luasnip'}
  use {'onsails/lspkind-nvim'}

  use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'}, {'nvim-lua/popup.nvim'} }
  }

  use {
    'hrsh7th/nvim-cmp',
    requires = {
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
    }
  }



end)
