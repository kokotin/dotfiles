require('neogen').setup({
        enabled = true,             -- required for Neogen to work
        input_after_comment = true, -- (default: true) automatic jump (with insert mode) on inserted annotation
    }
)
