local fn = vim.fn
local cmd = vim.api.nvim_command

function TrimWhitespace()
  local o = vim.o
  if not o.binary and o.filetype ~= 'diff' then
    local current_view = fn.winsaveview()
    cmd([[keeppatterns %s/\s\+$//e]])
    fn.winrestview(current_view)
  end
end

vim.cmd([[
augroup THE_PRIMEAGEN
    autocmd!
    autocmd BufWritePre * :lua TrimWhitespace()
    autocmd BufEnter,BufWinEnter,TabEnter *.rs :lua require'lsp_extensions'.inlay_hints{}
augroup END
]])

vim.cmd([[
augroup FormatAutogroup
  autocmd!
  autocmd BufWritePost *.py,*.lua FormatWrite
augroup END
]])

vim.cmd([[
fun! EmptyRegisters()
    let regs=split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/-"', '\zs')
    for r in regs
        call setreg(r, [])
    endfor
endfun
]])

vim.cmd([[
augroup highlight_yank
    autocmd!
    autocmd TextYankPost * silent! lua require'vim.highlight'.on_yank({timeout = 140})
augroup END
]])

vim.cmd([[filetype plugin indent on]])
vim.cmd([[syntax on]])

if not vim.g.vscode then
  require('debugui')
  require('identline')
  require('lsp')
  require('3sitter')
  require('funcontext')
  require('autopair')
  require('completions')
  require('sets')
  require('statusline')
  require('tabtabout')
  require('teleskop')
  require('whichkey')
  require('tree')
end
