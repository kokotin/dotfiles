lua<<EOF
require('formatter').setup({
  filetype = {
    lua = {
        -- luafmt
        function()
          return {
            exe = "luafmt",
            args = {"--indent-count", 2, "--stdin"},
            stdin = true
          }
        end
    },
  }
})
EOF

set tabstop=2 softtabstop=2
set shiftwidth=2
set autoindent
set smartindent
