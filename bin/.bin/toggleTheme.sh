#!/bin/sh

DARK_MODE=$(grep -w dark_mode "$HOME/.my_settings" | awk '{print $2}')

if [ "$DARK_MODE" -eq "0" ]; then
    #Dark
    sed -i "s|dark_mode 0|dark_mode 1|" "$HOME/.my_settings"

    #gtk3
    sed -i "s|gtk-icon-theme-name=Papirus|gtk-icon-theme-name=Dracula|" "$HOME/.config/gtk-3.0/settings.ini"
    sed -i "s|gtk-theme-name=Adwaita|gtk-theme-name=Dracula|" "$HOME/.config/gtk-3.0/settings.ini"

    #gtk2
    sed -i "s|gtk-icon-theme-name=\"Papirus\"|gtk-icon-theme-name=\"Dracula\"|" "$HOME/.gtkrc-2.0"
    sed -i "s|gtk-theme-name=\"Adwaita\"|gtk-theme-name=\"Dracula\"|" "$HOME/.gtkrc-2.0"

    #alacritty
    sed -i "s|colors: \*light|colors: \*dark|" "$HOME/.config/alacritty/alacritty.yml"

    #qt5ct
    sed -i "s|custom_palette=false|custom_palette=true|" "$HOME/.config/qt5ct/qt5ct.conf"
    sed -i "s|icon_theme=Papirus|icon_theme=Dracula|" "$HOME/.config/qt5ct/qt5ct.conf"
    sed -i "s|style=Breeze|style=gtk2|" "$HOME/.config/qt5ct/qt5ct.conf"
else
    #Light
    sed -i "s|dark_mode 1|dark_mode 0|" "$HOME/.my_settings"

    #gtk3
    sed -i "s|gtk-icon-theme-name=Dracula|gtk-icon-theme-name=Papirus|" "$HOME/.config/gtk-3.0/settings.ini"
    sed -i "s|gtk-theme-name=Dracula|gtk-theme-name=Adwaita|" "$HOME/.config/gtk-3.0/settings.ini"

    #gtk2
    sed -i "s|gtk-icon-theme-name=\"Dracula\"|gtk-icon-theme-name=\"Papirus\"|" "$HOME/.gtkrc-2.0"
    sed -i "s|gtk-theme-name=\"Dracula\"|gtk-theme-name=\"Adwaita\"|" "$HOME/.gtkrc-2.0"

    #alacritty
    sed -i "s|colors: \*dark|colors: \*light|" "$HOME/.config/alacritty/alacritty.yml"

    #qt5ct
    sed -i "s|custom_palette=true|custom_palette=false|" "$HOME/.config/qt5ct/qt5ct.conf"
    sed -i "s|icon_theme=Dracula|icon_theme=Papirus|" "$HOME/.config/qt5ct/qt5ct.conf"
    sed -i "s|style=gtk2|style=Breeze|" "$HOME/.config/qt5ct/qt5ct.conf"
fi

killall nm-applet







