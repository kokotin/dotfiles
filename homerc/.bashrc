#
# ~/.bashrc
#

#Ibus settings if you need them
#type ibus-setup in terminal to change settings and start the daemon
#delete the hashtags of the next lines and restart
#export GTK_IM_MODULE=ibus
#export XMODIFIERS=@im=dbus
#export QT_IM_MODULE=ibus

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export QT_QPA_PLATFORMTHEME=qt6ct
export FZF_DEFAULT_OPTS='--color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 --color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 --color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 --color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4'
export HISTCONTROL=ignoreboth:erasedups
export MANROFFOPT="-c"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export EDITOR="nvim"

PS1="\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"

if [ -d "$HOME/.bin" ]; then
	PATH="$HOME/.bin:$PATH"
fi

#list
# alias ls='ls -oAh --group-directories-first --color=auto'
alias ls='exa -lah --group-directories-first'
alias l='ls'
alias cat='bat -p'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#readable output
alias df='df -h'

#free
alias free="free -mt"

#continue download
alias wget="wget -c"

#userlist
alias userlist="cut -d: -f1 /etc/passwd"

#merge new settings
alias merge="xrdb -merge ~/.Xresources"

#startx
alias startx='startx 2> "$HOME"/.xsession-errors'
alias startw='exec sway 2> "$HOME"/.sway-log'

# Aliases for software managment
alias update='yay -Syu qt6gtk2 qt5gtk2 qt5-styleplugins --rebuild'
alias search='yay -Ss'

#Cleanup orphaned packages
alias cleanup='yay -Rns $(yay -Qtdq)'

#clean cache
alias cleancache='yay -Scc'

#remove
alias remove="yay -Rns"

#ps
alias ps="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#add new fonts
alias fc='sudo fc-cache -fv'

#hardware info --short
alias hw="hwinfo --short"

#check vulnerabilities microcode
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'

#shopt
shopt -s autocd  # Allows you to cd into directory merely by typing the directory name.
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend     # do not overwrite history
shopt -s expand_aliases # expand aliases

#youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "

alias ytv-best="youtube-dl -f bestvideo+bestaudio "

#get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

#shutdown now
alias ssn="sudo shutdown now"

#bare git
# alias gitt='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

#get fastest mirrors in your neighborhood
alias mirrorr="sudo reflector --verbose -l 200 -n 20 -p http --sort rate --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

#temperature
alias temp="sensors acpitz-acpi-0 | grep -w temp1"
alias tmux="TERM=xterm-256color tmux"

#bitwarden pass
bwpass() {
	bw get password "$1" | wl-copy -o
}

crtcheck() {
	openssl x509 -in "$1" -text -noout | rg -i "not after"
}

#command not found
source /usr/share/doc/pkgfile/command-not-found.bash

# pyenv
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

# starship promt (needs to be last)
eval "$(starship init bash)"
